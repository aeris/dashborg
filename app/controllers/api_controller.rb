class ApiController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    delete = params[:delete]

    info       = JSON.parse(request.raw_post).deep_symbolize_keys
    repository = info.fetch :repository
    name       = repository.fetch :location

    id         = repository.fetch :id
    repository = Repository.find_by_id id
    unless repository
      cache      = info.fetch :cache
      stats      = cache.fetch :stats
      size       = stats.fetch :total_csize
      real_size  = stats.fetch :total_size
      encryption = info.fetch :encryption
      mode       = encryption.fetch :mode
      repository = Repository.create! id:         id, name: name, size: size, real_size: real_size,
                                      encryption: mode
    end

    archives = info.fetch :archives
    ids      = []
    archives.each do |archive|
      ids << id = archive.fetch(:id)
      name       = archive.fetch :name
      stats      = archive.fetch :stats
      size       = stats.fetch :compressed_size
      real_size  = stats.fetch :original_size
      duration   = archive.fetch :duration
      created_at = archive.fetch :end

      args    = { name:     name, size: size, real_size: real_size,
                  duration: duration, created_at: created_at, purged_at: nil }
      archive = Archive.find_by_id id
      if archive
        archive.update! **args
      else
        repository.archives.create! id: id, **args
      end
    end

    repository.archives
              .where.not(id: ids)
              .update_all(purged_at: DateTime.now) if delete

    head :no_content
  rescue => e
    render json: e
  end
end
