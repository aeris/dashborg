class CreateRepositories < ActiveRecord::Migration[7.0]
  def change
    create_table :repositories, id: :string do |t|
      t.string :name
      t.string :encryption
      t.integer :size, limit: 8, null: false
      t.integer :real_size, limit: 8, full: false
    end
  end
end
